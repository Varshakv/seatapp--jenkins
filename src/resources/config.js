const env = process.env.NODE_ENV || 'development';

//CONFIGURATION FOR DEV ENVIRONMENT
const development = {
    backendHN: 'http://localhost',
    backendPort: 8000,
    viewerHN: 'http://localhost',
    viewerPort: 8087,
    env: process.env.NODE_ENV
};


//PLEASE FILL THE VALUES FOR PRODUCTION ENVIRONMENT
const production = {
    backendHN: 'https://myseatfinder.techmahindra.com',
    backendPort: 8000,
    viewerHN: 'https://myseatfinder.techmahindra.com',
    viewerPort: 8081
};

//PLEASE FILL THE VALUES FOR TEST ENVIRONMENT
const test = {
    backendHN: '',
    backendPort: null,
    viewerHN: '',
    viewerPort: null
};

const endpoints = {
    development,
    production,
    test
};

module.exports = endpoints[env];
import React, { Component } from 'react';
import LoginComponent from './components/LoginComponent';
import BasicComponent from './components/BasicComponent';
import AdvancedComponent from './components/AdvancedComponent';
import ViewBookingsComponent from './components/ViewBookingsComponent';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';

class App extends Component {
    constructor(props){
      super(props);

      this.openNav = this.openNav.bind(this);
      this.closeNav = this.closeNav.bind(this);
      this.LoginToBasic = this.LoginToBasic.bind(this);
      this.responseFromBasic = this.responseFromBasic.bind(this);
      this.responseFromAdvanced = this.responseFromAdvanced.bind(this);
      this.goToBasic = this.goToBasic.bind(this);
      this.goToAdvanced = this.goToAdvanced.bind(this);
      this.goToViewBookings = this.goToViewBookings.bind(this);
      this.logout = this.logout.bind(this);

      this.state = {
        lanid: '',
        username: '',
        customer_group: '',
        showLoginComponent: true,
        showBasicOptionsComponent: false,
        showAdvancedOptionsComponent: false,
        showViewBookingsComponent: false
      };
    }

    openNav(e){
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("main").style.marginLeft = "0px";
    }

    closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      document.getElementById("main").style.marginLeft = "0";
    }

    LoginToBasic(dataFromLogin){
      this.setState({
        lanid: dataFromLogin.emailid.substr(0, 10),
        username: dataFromLogin.username,
        customer_group: dataFromLogin.customer_group,
        showLoginComponent: false,
        showBasicOptionsComponent: true
      });
      document.getElementById('main').style.display = 'block';
    }

    responseFromBasic(data){
      this.setState({showBasicOptionsComponent: false});
      if(data === 'ViewAdvanced'){
        this.setState({showAdvancedOptionsComponent: true});
      } else {
        this.setState({showViewBookingsComponent: true});
      }
    }

    responseFromAdvanced(data){
      this.setState({showAdvancedOptionsComponent: false});
      if(data === 'ViewBasic'){
        this.setState({showBasicOptionsComponent: true});
      } else {
        this.setState({showViewBookingsComponent: true});
      }
    }

    goToBasic(e){
      this.setState({
        showLoginComponent: false,
        showAdvancedOptionsComponent: false,
        showViewBookingsComponent: false,
        showBasicOptionsComponent: true
      });
      this.closeNav();
    }

    goToAdvanced(e){
      this.setState({
        showLoginComponent: false,
        showBasicOptionsComponent: false,
        showViewBookingsComponent: false,
        showAdvancedOptionsComponent: true 
      });
      this.closeNav();
    }

    goToViewBookings(e){
      this.setState({
        showLoginComponent: false,
        showBasicOptionsComponent: false,
        showAdvancedOptionsComponent: false,
        showViewBookingsComponent: true
      });
      this.closeNav();
    }

    logout(e){
      this.setState({
        lanid: '',
        showLoginComponent: true,
        showBasicOptionsComponent: false,
        showAdvancedOptionsComponent: false,
        showViewBookingsComponent: false
      });
      document.getElementById('main').style.display = 'none';
    }

    render(){
      return(
        <div className="app">
          { this.state.showLoginComponent ? <LoginComponent callbackFromApp={this.LoginToBasic} /> : null}
          <div id="main" style={{display: 'none'}}>
            <nav className="navbar navbar-light bg-light">
              <button onClick={this.openNav} className="btn"><i className="fa fa-bars"></i></button>
              <h6><p style={{ color:"#ffffff", fontSize:"large" , marginTop:8}}>My Seat Finder Application</p></h6>
              <div className="my-2 my-md-0">
                <div className="navbar-brand">
                  <button className="btn logout-btn" onClick={this.logout}><h6>Logout</h6></button>
                </div>
              </div>
            </nav>
            <div id="mySidenav" className="sidenav">
              <button className="btn btn-link closebtn" onClick={this.closeNav}>&times;</button>
              <button className="btn btn-link navbtn" onClick={this.goToBasic}><h6>Basic Mode</h6></button>
              <button className="btn btn-link navbtn" onClick={this.goToAdvanced}><h6>Advanced Mode</h6></button>
              <button className="btn btn-link navbtn" onClick={this.goToViewBookings}><h6>View Bookings</h6></button>
            </div>
            { this.state.showBasicOptionsComponent ? <BasicComponent userinfo={{lanid: this.state.lanid, username: this.state.username, customer_group: this.state.customer_group}} callbackFromApp={this.responseFromBasic} /> : null }
            { this.state.showAdvancedOptionsComponent ? <AdvancedComponent userinfo={{lanid: this.state.lanid, username: this.state.username, customer_group: this.state.customer_group}} callbackFromApp={this.responseFromAdvanced} /> : null }
            { this.state.showViewBookingsComponent ? <ViewBookingsComponent infoToVBC={this.state.lanid} /> : null }
          </div>
        </div>
      );
    }
}

export default App;

import React, { Component } from 'react';
import axios from 'axios';
import endpoints from '../resources/config';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class BasicComponent extends Component {
    constructor(props) {
        super(props);

        this.onStartDateChange = this.onStartDateChange.bind(this);
        this.onEndDateChange = this.onEndDateChange.bind(this);
        this.onCitySelected = this.onCitySelected.bind(this);
        this.onLocationSelected = this.onLocationSelected.bind(this);
        this.onBuildingSelected = this.onBuildingSelected.bind(this);
        this.onBookClick = this.onBookClick.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.bookSeat = this.bookSeat.bind(this);
        this.goToViewBookings = this.goToViewBookings.bind(this);
        this.onResetClick = this.onResetClick.bind(this);
        this.goToAdvanced = this.goToAdvanced.bind(this);
        this.getCurrentLocationDetails = this.getCurrentLocationDetails.bind(this);
        this.geolocationErrorHandler = this.geolocationErrorHandler.bind(this);

        this.listCities = this.listCities.bind(this);
        this.listLocations = this.listLocations.bind(this);
        this.listBuildings = this.listBuildings.bind(this);

        this.state = {
            lanid: '',
            username: '',
            baseLocation: '',
            currentCity: '',
            currentLocation: '',
            today: '',
            maxSDate: '',
            maxEDate: '',
            startDate: '',
            endDate: '',
            cities: [],
            selectedCity: '',
            locations: [],
            selectedLocation: '',
            buildings: [],
            selectedBuilding: '',
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: '',
            customerGroup: '',
            showModal: false
        };
    }

    getCurrentLocationDetails(position){
      let lat = position.coords.latitude.toFixed(2);
      let long = position.coords.longitude.toFixed(2);
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/custom/getCurrentLocation/' + lat + '/' + long)
            .then(response => {
                this.setState({
                    currentCity: response.data.city,
                    currentLocation: response.data.locationname,
                    selectedCity: response.data.city,
                    selectedLocation: response.data.locationname,
                });
                let geoCity = response.data.city;
                let geoLocation = response.data.locationname;
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + "custom/getUserDetails/" + this.props.userinfo.lanid.substr(4, 6))
                    .then(response => {
                        this.setState({
                            baseLocation: response.data.locationname
                        });
                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
                            .then(response => {
                                this.setState({ cities: response.data });
                                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + geoCity)
                                    .then(response => {
                                        this.setState({ locations: response.data });
                                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + geoLocation)
                                            .then(response => {
                                                this.setState({ buildings: response.data });
                                            }).catch(err => {
                                                console.log(err);
                                            });
                                    }).catch(err => {
                                        console.log(err);
                                    });
                            }).catch(err => {
                                console.log(err);
                            });
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
    }

    geolocationErrorHandler() {
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + "custom/getUserDetails/" + this.props.userinfo.lanid.substr(4, 6))
            .then(response => {
                this.setState({
                    baseLocation: response.data.locationname
                });
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
                    .then(response => {
                        this.setState({ cities: response.data });
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
    }

    componentDidMount() {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        let dateString = yyyy + "-" + mm + "-" + dd;
        let maxForSD = new Date(dateString);
        maxForSD.setDate(today.getDate() + 14);
        let date = maxForSD.getDate();
        let month = maxForSD.getMonth() + 1;
        let year = maxForSD.getFullYear();
        if (date < 10) {
            date = '0' + date;
        }
        if (month < 10) {
            month = '0' + month;
        }
        let maxDateString = year + '-' + month + '-' + date;
        this.setState({
            lanid: this.props.userinfo.lanid,
            username: this.props.userinfo.username,
            customerGroup: this.props.userinfo.customer_group,
            today: dateString,
            startDate: dateString,
            endDate: dateString,
            maxSDate: maxDateString,
            maxEDate: maxDateString
        });
        
        navigator.geolocation.getCurrentPosition(this.getCurrentLocationDetails, this.geolocationErrorHandler);
    }

    onStartDateChange(e) {
        let selectedStartDate = new Date(e.target.value);
        selectedStartDate.setDate(selectedStartDate.getDate() + 14);
        let dd = selectedStartDate.getDate();
        let mm = selectedStartDate.getMonth() + 1;
        let yyyy = selectedStartDate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        let dateString = yyyy + '-' + mm + '-' + dd;
        this.setState({
            startDate: e.target.value,
            endDate: e.target.value,
            maxEDate: dateString,
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: ''
        });
    }

    onEndDateChange(e) {
        this.setState({
            endDate: e.target.value,
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: ''
        });
    }

    onCitySelected(e) {
        this.setState({
            selectedCity: e.target.value,
            locations: [],
            buildings: []
        });
        document.getElementById('anyOptionForBuildings').style.display = 'none';
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + e.target.value)
            .then(response => {
                this.setState({
                    selectedLocation: '',
                    selectedBuilding: '',
                    returnedSpaceType: '',
                    returnedSpaceId: '',
                    returnedWing: '',
                    returnedFloor: '',
                    returnedBuilding: '',
                    returnedSpaceDetailId: '',
                    locations: response.data
                });
            }).catch(err => {
                console.log(err);
            });
    }

    onLocationSelected(e) {
        this.setState({
            selectedLocation: e.target.value,
            selectedBuilding: '',
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: '',
            buildings: []
        });
        if (e.target.value === '') {
            document.getElementById('anyOptionForBuildings').style.display = 'none';
        } else {
            document.getElementById('anyOptionForBuildings').style.display = 'block';
            axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + e.target.value)
                .then(response => {
                    this.setState({
                        buildings: response.data
                    });
                }).catch(err => {
                    console.log(err);
                });
        }
    }

    onBuildingSelected(e) {
        this.setState({
            selectedBuilding: e.target.value,
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: ''
        });
    }

    onBookClick(e) {
        e.preventDefault();
        var info = {
            selectedLocation: this.state.selectedLocation,
            selectedBuilding: this.state.selectedBuilding,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            customerGroup: this.state.customerGroup
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getSeatsBasic', info)
            .then(response => {
                this.setState({ showModal: true });
                if (response.data.length === 0) {
                    document.getElementById("noSpacesMsg").style.display = 'block';
                    document.getElementById("noSpacesBox").style.display = 'block';
                } else {
                    this.setState({
                        returnedSpaceId: response.data[0].spaceid,
                        returnedSpaceType: response.data[0].spacetype,
                        returnedWing: response.data[0].wing,
                        returnedFloor: response.data[0].floor,
                        returnedBuilding: response.data[0].building,
                        returnedSpaceDetailId: response.data[0].spacedetailid
                    });
                    document.getElementById('confirmationMsg').style.display = 'block';
                    document.getElementById('confirmationBox').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
            });
    }

    bookSeat(e) {
        var info = {
            "bookingrequestnumber": Math.floor(Date.now() / 1000),
            "associateid": this.state.lanid.substr(4, 6),
            "bookingstartdate": this.state.startDate,
            "bookingenddate": this.state.endDate,
            "spaceid": this.state.returnedSpaceId,
            "location": this.state.selectedLocation,
            "spacedetailid": this.state.returnedSpaceDetailId
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/book', info)
            .then(response => {
                if (response.data === 1) {
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('alreadyBookedMsg').style.display = 'block';
                    document.getElementById('alreadyBookedBox').style.display = 'block';
                } else if (response.data === 2) {
                    //mailer code
                    var mailerObject = {
                        recipient: this.state.lanid,
                        spaceType: this.state.returnedSpaceType,
                        spaceid: this.state.returnedSpaceId,
                        building: this.state.returnedBuilding,
                        wing: this.state.returnedWing,
                        floor: this.state.returnedFloor,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        locationName: this.state.selectedLocation,
                        username: this.state.username,
                        brn: info.bookingrequestnumber
                    };
                    axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'mail', mailerObject)
                        .then(response => {
                            (response.data === 1 ? console.log("Mail sent") : console.log("Mail not sent"));
                        }).catch(err => {
                            console.log(err);
                        });
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('postBookingMsg').style.display = 'block';
                    document.getElementById('postBookingBox').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
            });

    }

    onResetClick(e) {
        e.preventDefault();
        this.setState({
            startDate: this.state.today,
            endDate: this.state.today,
            selectedCity: this.state.currentCity,
            selectedLocation: this.state.currentLocation,
            selectedBuilding: "",
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedWing: '',
            returnedFloor: '',
            returnedBuilding: '',
            returnedSpaceDetailId: ''
        });
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
            .then(response => {
                this.setState({ cities: response.data });
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + this.state.currentCity)
                    .then(response => {
                        this.setState({ locations: response.data });
                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + this.state.currentLocation)
                            .then(response => {
                                this.setState({ buildings: response.data });
                                document.getElementById('anyOptionForBuildings').style.display = 'block';
                            }).catch(err => {
                                console.log(err);
                            });
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
    }

    goToViewBookings(e) {
        this.props.callbackFromApp('ViewBookings');
    }

    goToAdvanced(e) {
        e.preventDefault();
        this.props.callbackFromApp('ViewAdvanced');
    }

    toggleModal(e) {
        this.setState({ showModal: !this.state.showModal });
    }

    listCities() {
        return this.state.cities.map(function (city, i) {
            return (
                <option key={i} value={city}>{city}</option>
            );
        });
    }

    listLocations() {
        return this.state.locations.map(function (location, i) {
            return (
                <option key={i} value={location}>{location}</option>
            );
        });
    }

    listBuildings() {
        return this.state.buildings.map(function (building, i) {
            return (
                <option key={i} value={building}>{building}</option>
            );
        });
    }

    render() {
        return (
            <section className="container" style={{ marginTop: 10 }}>
                <div className="row" style={{ marginBottom: 20 }}>
                    <div className="col-sm-12 col-xs-12">
                        <h6>
                            <p style={{ textAlign: 'center', color: " #8E8E8E", fontWeight: 'bold' }}>
                                Welcome to My Seat Finder Application, <span style={{ color: '#716869' }}>{this.state.username}.</span>  Your base location is <span style={{ color: '#716869' }}>{this.state.baseLocation}</span>.  {this.state.currentCity ? "You are now in " + this.state.currentCity + "." : ''}
                            </p>
                        </h6>
                    </div>
                </div>
                <form>
                    <div className="form-row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>From</h6></label>
                            <input type="date" className="form-control" onChange={this.onStartDateChange} min={this.state.today} max={this.state.maxSDate} value={this.state.startDate} required />
                        </div>
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>To</h6></label>
                            <input type="date" className="form-control" onChange={this.onEndDateChange} min={this.state.startDate} max={this.state.maxEDate} value={this.state.endDate} required />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>City</h6></label>
                            <select className="form-control" onChange={this.onCitySelected} value={this.state.selectedCity} required>
                                <option value="">Select City</option>
                                {this.listCities()}
                            </select>
                        </div>
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Location</h6></label>
                            <select className="form-control" onChange={this.onLocationSelected} value={this.state.selectedLocation} required>
                                <option value="">Select Location</option>
                                {this.listLocations()}
                            </select>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Building</h6></label>
                            <select className="form-control" onChange={this.onBuildingSelected} value={this.state.selectedBuilding} required>
                                <option value="">Select Building</option>
                                <option value={'ANY'} id="anyOptionForBuildings">Any</option>
                                {this.listBuildings()}
                            </select>
                        </div>
                    </div>
                    <div className="form-row justify-content-center">
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" disabled={this.state.selectedBuilding ? false : true} style={{ margin: 5 }} onClick={this.onBookClick}>Book Seat</button>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{ margin: 5 }} onClick={this.onResetClick}>Reset</button>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{ margin: 5 }} onClick={this.goToAdvanced} title="Click here for more options">Advanced Mode</button>
                        </div>
                    </div>
                </form>
                <div className="row justify-content-center" style={{ marginTop: 25 }}>
                    <p>Click on Advanced Mode for more options</p>
                </div>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Seat Booking Confirmation</ModalHeader>
                    <ModalBody>
                        <div id="alreadyBookedMsg" style={{ display: 'none' }}>
                            <p className="justify-content-center">
                                You already have booked a seat or have an ongoing booking in between  {this.state.startDate} and {this.state.endDate}.
                            </p>
                        </div>
                        <div id="confirmationMsg" style={{ display: 'none' }}>
                            <p className="justify-content-center">
                                Click Yes to book {this.state.returnedSpaceType} {this.state.returnedSpaceId} in {this.state.returnedWing === 'NA' ? this.state.returnedFloor + ' ODC' : this.state.returnedWing}, {this.state.returnedFloor}, {this.state.returnedBuilding} from {this.state.startDate} till {this.state.endDate}.
                            </p>
                        </div>
                        <div id="noSpacesMsg" style={{ display: 'none' }}>
                            <p className="justify-content-center">Sorry, there are no spaces in {this.state.selectedBuilding === 'ANY' ? this.state.selectedLocation : this.state.selectedBuilding}.  Please try the Advanced Mode for more options</p>
                        </div>
                        <div id="postBookingMsg" style={{ display: 'none' }}>
                            <p className="justify-content-center">
                                Space {this.state.returnedSpaceId} has been booked for you from {this.state.startDate} till {this.state.endDate}.  The booking details will be mailed to you shortly.
                            </p>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <div id="confirmationBox" style={{ display: 'none' }}>
                            <button className="btn btn-techm-red" style={{ margin: 5 }} onClick={this.toggleModal}>No</button>
                            <button className="btn btn-primary" style={{ margin: 5 }} onClick={this.bookSeat}>Yes</button>
                        </div>
                        <div id="noSpacesBox" style={{ display: 'none' }}>
                            <button className="btn btn-techm-red" style={{ margin: 5 }} onClick={this.toggleModal}>&times; Close</button>
                        </div>
                        <div id="postBookingBox" style={{ display: 'none' }}>
                            <button className="btn btn-primary" style={{ margin: 5 }} onClick={this.goToViewBookings}>View Bookings</button>
                        </div>
                        <div id="alreadyBookedBox" style={{ display: 'none' }}>
                            <button className="btn btn-primary" style={{ margin: 5 }} onClick={this.toggleModal}>OK</button>
                        </div>
                    </ModalFooter>
                </Modal>
            </section>
        );
    }
}
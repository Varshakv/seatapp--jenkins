import React, { Component } from 'react';
import axios from 'axios';
import endpoints from '../resources/config';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SeatListComponent from './SeatListComponent';
import SeatViewerComponent from './SeatViewerComponent';

export default class AdvancedComponent extends Component{
    constructor(props){
        super(props);

        this.onStartDateChange = this.onStartDateChange.bind(this);
        this.onEndDateChange = this.onEndDateChange.bind(this);
        this.onCitySelected = this.onCitySelected.bind(this);
        this.onLocationSelected = this.onLocationSelected.bind(this);
        this.onBuildingSelected = this.onBuildingSelected.bind(this);
        this.onFloorSelected = this.onFloorSelected.bind(this);
        this.onWingSelected = this.onWingSelected.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.onBookClick = this.onBookClick.bind(this);
        this.bookSeat = this.bookSeat.bind(this);
        this.goToViewBookings = this.goToViewBookings.bind(this);
        this.onResetClick = this.onResetClick.bind(this);
        this.goToBasic = this.goToBasic.bind(this);
        this.showSeats = this.showSeats.bind(this);
        this.responseFromSLC = this.responseFromSLC.bind(this);
        this.getCurrentPositionDetails = this.getCurrentPositionDetails.bind(this);
        this.geoLocationErrorHandler = this.geoLocationErrorHandler.bind(this);
        

        this.listCities = this.listCities.bind(this);
        this.listLocations = this.listLocations.bind(this);
        this.listBuildings = this.listBuildings.bind(this);
        this.listFloors = this.listFloors.bind(this);
        this.listWings = this.listWings.bind(this);
        this.listSeats = this.listSeats.bind(this);
        this.listSeatRows = this.listSeatRows.bind(this);

        this.state = {
            lanid: '',
            username: '',
            baseLocation: '',
            startDate: '',
            endDate: '',
            maxSDate: '',
            maxEDate: '',
            currentCity: '',
            currentLocation: '',
            cities: [],
            selectedCity: '',
            locations: [],
            selectedLocation: '',
            buildings: [],
            selectedBuilding: '',
            returnedBuilding: '',
            floors: [],
            selectedFloor: '',
            returnedFloor: '',
            wings: [],
            selectedWing: '',
            returnedWing: '',
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedSpaceDetailId: '',
            returnedCustomerGroup: '',
            spaces: [],
            customerGroup: '',
            showModal: false
        };
    }

    getCurrentPositionDetails(position){
        let lat = position.coords.latitude.toFixed(2);
        let long = position.coords.longitude.toFixed(2);
    
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/custom/getCurrentLocation/' + lat + '/' + long)
            .then(response => {
                this.setState({
                    currentCity: response.data.city,
                    currentLocation: response.data.locationname,
                    selectedCity: response.data.city,
                    selectedLocation: response.data.locationname
                });
                let geoCity = response.data.city;
                let geoLocation = response.data.locationname;
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getUserDetails/' + this.props.userinfo.lanid.substr(4, 6))
                    .then(response => {
                        this.setState({
                            baseLocation: response.data.locationname
                        });
                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
                            .then(response => {
                                this.setState({cities: response.data});
                                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + geoCity)
                                    .then(response => {
                                        this.setState({locations: response.data});
                                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + geoLocation)
                                            .then(response => {
                                                this.setState({buildings: response.data});
                                            }).catch(err => {
                                                console.log(err);
                                            });
                                    }).catch(err => {
                                        console.log(err);
                                    });
                            }).catch(err => {
                                console.log(err);
                            });
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
    }

    geoLocationErrorHandler(){
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getUserDetails/' + this.props.userinfo.lanid.substr(4, 6))
            .then(response => {
                this.setState({
                    baseLocation: response.data.locationname
                });
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
                    .then(response => {
                        this.setState({cities: response.data});
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
    }

    componentDidMount(){
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        }
        if(mm < 10){
            mm = '0' + mm;
        }
        let dateString = yyyy + '-' + mm + '-' + dd;
        let maxForSD = new Date(dateString);
        maxForSD.setDate(maxForSD.getDate() + 14);
        let date = maxForSD.getDate();
        let month = maxForSD.getMonth() + 1;
        let year = maxForSD.getFullYear();
        if(date < 10){
            date = '0' + date;
        }
        if(month < 10){
            month = '0' + month;
        }
        let maxDateString = year + '-' + month + '-' + date;
        this.setState({
            lanid: this.props.userinfo.lanid,
            username: this.props.userinfo.username,
            customerGroup: this.props.userinfo.customer_group,
            today: dateString,
            startDate: dateString,
            endDate: dateString,
            maxSDate: maxDateString,
            maxEDate: maxDateString
        });
        navigator.geolocation.getCurrentPosition(this.getCurrentPositionDetails, this.geoLocationErrorHandler);
    }

    onStartDateChange(e){
        let sd = new Date(e.target.value);
        let dd = sd.getDate() + 14;
        let mm = sd.getMonth() + 1;
        let yyyy = sd.getFullYear();
        if(dd < 10) {
            dd = '0' + dd;
        }
        if(mm < 10) {
            mm = '0' + mm;
        }
        let sdString = yyyy + '-' + mm + '-' + dd;
        this.setState({
            startDate: e.target.value,
            endDate: e.target.value,
            spaces: [],
            maxEDate: sdString,
            returnedCustomerGroup: '',
            returnedSpaceDetailId: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            selectedLocation: '',
            selectedBuilding: '',
            selectedFloor: '',
            selectedWing: '',
            locations: [],
            buildings: [],
            floors: [],
            wings: []
        });
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + this.state.currentCity)
            .then(response => {
                this.setState({locations: response.data});
            }).catch(err => {
                console.log(err);
            });
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
    }

    onEndDateChange(e){
        this.setState({
            endDate: e.target.value,
            spaces: [],
            returnedCustomerGroup: '',
            returnedSpaceDetailId: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            selectedLocation: '',
            selectedBuilding: '',
            selectedFloor: '',
            selectedWing: '',
            locations: [],
            buildings: [],
            floors: [],
            wings: []
        });
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + this.state.currentCity)
            .then(response => {
                this.setState({locations: response.data});
            }).catch(err => {
                console.log(err);
            });
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
    }

    onCitySelected(e){
        this.setState({
            selectedLocation: '',
            selectedBuilding: '',
            selectedWing: '',
            selectedFloor: '',
            returnedCustomerGroup: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            returnedSpaceDetailId: '',
            locations: [],
            buildings: [],
            wings: [],
            floors: [],
            spaces: [],
            selectedCity: e.target.value
        });
        document.getElementById('anyOptionForFloor').style.display = 'none';
        document.getElementById('anyOptionForWing').style.display = 'none';
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + e.target.value)
            .then(response => {
                this.setState({locations: response.data});
            }).catch(err => {
                console.log(err);
            });
    }

    onLocationSelected(e){
        this.setState({
            selectedBuilding: '',
            selectedWing: '',
            selectedFloor: '',
            returnedCustomerGroup: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            returnedSpaceDetailId: '',
            buildings: [],
            wings: [],
            floors: [],
            spaces: [],
            selectedLocation: e.target.value
        });
        document.getElementById('anyOptionForFloor').style.display = 'none';
        document.getElementById('anyOptionForWing').style.display = 'none';
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
        if(e.target.value !== ''){
            axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + e.target.value)
            .then(response => {
                this.setState({buildings: response.data});
            }).catch(err => {
                console.log(err);
            });
        }
    }

    onBuildingSelected(e){
        this.setState({
            selectedFloor: '',
            selectedWing: '',
            returnedCustomerGroup: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            returnedSpaceDetailId: '',
            floors: [],
            wings: [],
            spaces: [],
            selectedBuilding: e.target.value
        });
        document.getElementById('anyOptionForWing').style.display = 'none';
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
        if(e.target.value === ''){
            document.getElementById('anyOptionForFloor').style.display = 'none';
        } else {
            document.getElementById('anyOptionForFloor').style.display = 'block';
            axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getFloors/' + e.target.value + '/' + this.state.selectedLocation)
            .then(response => {
                this.setState({floors: response.data});
            }).catch(err => {
                console.log(err);
            });
        }
    }

    onFloorSelected(e){
        this.setState({
            selectedWing: '',
            returnedCustomerGroup: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            returnedSpaceDetailId: '',
            wings: [],
            spaces: [],
            selectedFloor: e.target.value
        });
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
        document.getElementById('anyOptionForWing').style.display = 'block';
        if(e.target.value !== ''){
            if(e.target.value === 'ANY'){
                this.setState({selectedWing: 'ANY'});
                document.getElementById('wingSelector').value = 'ANY';
                var info = {
                    selectedLocation: this.state.selectedLocation,
                    selectedBuilding: this.state.selectedBuilding,
                    selectedFloor: e.target.value,
                    selectedWing: e.target.value,
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                };
                axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getSeatsAdvanced', info)
                    .then(response => {
                        if(response.data.length > 0){
                            this.setState({
                                spaces: response.data
                            });
                        }
                    }).catch(err => {
                        console.log(err);
                    });
            } else {
                document.getElementById('wingSelector').value = '';
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getWings/' + e.target.value + '/' + this.state.selectedBuilding + '/' + this.state.selectedLocation)
                    .then(response => {
                        for(var i = 0;i < response.data.length; i++){
                            if(response.data[i] === 'NA'){
                                response.data.splice(i, 1);
                            }
                        }
                        this.setState({wings: response.data});
                    }).catch(err => {
                        console.log(err);
                    });
            }
        } else {
            document.getElementById('anyOptionForWing').style.display = 'none';
        }
    }

    onWingSelected(e){
        this.setState({
            selectedWing: e.target.value,
            spaces: [],
            returnedCustomerGroup: '',
            returnedSpaceId: '',
            returnedSpaceType: '',
            returnedBuilding: '',
            returnedFloor: '',
            returnedWing: '',
            returnedSpaceDetailId: ''
        });
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
        var info = {
            selectedLocation: this.state.selectedLocation,
            selectedBuilding: this.state.selectedBuilding,
            selectedFloor: this.state.selectedFloor,
            selectedWing: e.target.value,
            startDate: this.state.startDate,
            endDate: this.state.endDate
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getSeatsAdvanced', info)
            .then(response => {
                if(response.data.length > 0){
                    this.setState({
                        spaces: response.data
                    });
                }
            }).catch(err => {
                console.log(err);
            });
    }

    toggleModal(e){
        this.setState({showModal: !this.state.showModal});
    }

    onBookClick(e){
        e.preventDefault();
        var info = {
            selectedLocation: this.state.selectedLocation,
            selectedBuilding: this.state.selectedBuilding,
            selectedFloor: this.state.selectedFloor,
            selectedWing: this.state.selectedWing,
            startDate: this.state.startDate,
            endDate: this.state.endDate
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getSeatsAdvanced', info)
            .then(response => {
                this.setState({showModal: true});
                if(response.data.length === 0){
                    document.getElementById('noSpacesMsg').style.display = 'block';
                    document.getElementById('noSpacesBox').style.display = 'block';
                } else {
                    this.setState({
                        returnedSpaceId: response.data[0].spaceid,
                        returnedSpaceType: response.data[0].spacetype,
                        returnedWing: response.data[0].wing,
                        returnedFloor: response.data[0].floor,
                        returnedBuilding: response.data[0].building,
                        returnedSpaceDetailId: response.data[0].spacedetailid,
                        returnedCustomerGroup: response.data[0].customer_group
                    });
                    document.getElementById('confirmationMsg').style.display = 'block';
                    document.getElementById('confirmationBox').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
            });
    }

    bookSeat(e){
        var info = {
            "bookingrequestnumber": Math.floor(Date.now() / 1000),
            "associateid": this.state.lanid.substr(4, 6),
            "bookingstartdate": this.state.startDate,
            "bookingenddate": this.state.endDate,
            "spaceid": this.state.returnedSpaceId,
            "location": this.state.selectedLocation,
            "spacedetailid": this.state.returnedSpaceDetailId
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/book', info)
            .then(response => {
                if(response.data === 1){
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('alreadyBookedMsg').style.display = 'block';
                    document.getElementById('alreadyBookedBox').style.display = 'block';
                } else if(response.data === 2){
                    var mailerObject = {
                        brn: info.bookingrequestnumber,
                        recipient: this.state.lanid,
                        spaceType: this.state.returnedSpaceType,
                        spaceid: this.state.returnedSpaceId,
                        building: this.state.returnedBuilding,
                        wing: this.state.returnedWing,
                        floor: this.state.returnedFloor,
                        startDate: this.state.startDate,
                        endDate: this.state.endDate,
                        locationName: this.state.selectedLocation,
                        username: this.state.username
                    };
                    axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'mail', mailerObject)
                        .then(response => {
                            (response.data === 1 ? console.log("Mail sent") : console.log("Mail not sent"));
                        }).catch(err => {
                            console.log(err);
                        });
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('postBookingMsg').style.display = 'block';
                    document.getElementById('postBookingBox').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
            });
    }

    goToViewBookings(){
        this.props.callbackFromApp('ViewBookings');
    }

    onResetClick(e){
        e.preventDefault();
        this.setState({
            startDate: this.state.today,
            endDate: this.state.today,
            selectedCity: this.state.currentCity,
            selectedLocation: this.state.currentLocation,
            selectedBuilding: this.state.currentBuilding,
            returnedBuilding: '',
            buildings: [],
            locations: [],
            floors: [],
            cities: [],
            selectedFloor: '',
            returnedFloor: '',
            wings: [],
            selectedWing: '',
            returnedWing: '',
            returnedSpaceType: '',
            returnedSpaceId: '',
            returnedCustomerGroup: '',
            spaces: [],
        });
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueCities')
            .then(response => {
                this.setState({cities: response.data})
                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/uniqueLocations/' + this.state.currentCity)
                    .then(response => {
                        this.setState({locations: response.data});
                        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getBuildings/' + this.state.currentLocation)
                            .then(response => {
                                this.setState({buildings: response.data});
                                axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getFloors/' + this.state.currentBuilding + '/' + this.state.currentLocation)
                                    .then(response => {
                                        this.setState({floors: response.data});
                                        document.getElementById('anyOptionForWing').style.display = 'none';
                                    }).catch(err => {
                                        console.log(err);
                                    });
                            }).catch(err => {
                                console.log(err);
                            });
                    }).catch(err => {
                        console.log(err);
                    });
            }).catch(err => {
                console.log(err);
            });
        document.getElementById('listSeats').style.display = 'none';
        document.getElementById('viewSeatSection').style.display = 'none';
    }

    showSeats(e){
        e.preventDefault();
        document.getElementById('listSeats').style.display = 'block';
    }

    goToBasic(e){
        e.preventDefault();
        this.props.callbackFromApp('ViewBasic');
    }

    listCities(){
        return this.state.cities.map(function(city, i){
            return(
                <option key={i} value={city}>{city}</option>
            );
        });
    }

    listLocations(){
        return this.state.locations.map(function(location, i){
            return(
                <option key={i} value={location}>{location}</option>
            );
        });
    }

    listBuildings(){
        return this.state.buildings.map(function(building, i){
            return(
                <option key={i} value={building}>{building}</option>
            );
        });
    }

    listFloors(){
        return this.state.floors.map(function(floor, i){
            return(
                <option key={i} value={floor}>{floor}</option>
            );
        })
    }

    listWings(){
        return this.state.wings.map(function(wing, i){
            return(
                <option key={i} value={wing}>{wing}</option>
            );
        });
    }

    listSeats(){
        if(this.state.spaces.length === 0){
            return(
                <div style={{marginTop: 20, textAlign: 'center'}}>
                    <h6><p>Sorry, there are no seats in {this.state.selectedFloor === 'ANY' ? this.state.selectedFloor + ' floor' : this.state.selectedFloor}, {this.state.selectedWing === 'ANY' ? this.state.selectedWing + ' wing' : this.state.selectedWing}, {this.state.selectedBuilding}</p></h6>
                </div>
            );
        } else {
            return(
                <div>
                    <div style={{margin: 20}}>
                        <h6><p style={{textAlign: 'center'}}>Seats available in {this.state.selectedBuilding}</p></h6>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="thead-light">
                                <tr style={{textAlign: 'center', fontSize: 'small'}}>
                                    <th>SpaceID</th>
                                    <th>Space Type</th>
                                    <th>Location</th>
                                    <th>Building</th>
                                    <th>Floor</th>
                                    <th>Wing</th>
                                    <th colSpan={2}>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.listSeatRows()}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }

    listSeatRows(){
        return this.state.spaces.map(function(space, i){
            var infoToSLC = {
                lanid: this.state.lanid,
                space: {space},
                startDate: this.state.startDate,
                endDate: this.state.endDate,
                location: this.state.selectedLocation,
                username: this.state.username,
                customerGroup: this.state.customerGroup
            };
            return <SeatListComponent infoToSLC={infoToSLC} callbackFromAdvanced={this.responseFromSLC} key={i} />;
        }, this);
    }

    responseFromSLC(data){
        if(data.id === 'ViewSeat'){
            this.setState({sidToViewer: data.spaceid});
            document.getElementById('viewSeatSection').style.display = 'block';
        } else if(data.id === 'ViewBookings'){
            this.props.callbackFromApp('ViewBookings');
        }
    }

    render(){
        return(
            <section className="container container-fluid" style={{marginTop: 10}}>
                <div className="row">
                    <div className="col-sm-12 col-xs-12">
                        <h6>
                            <p style={{textAlign: 'center', color:" #8E8E8E" , fontWeight: 'bold'}}>
                            Welcome to My Seat Finder Application, <span style={{color: '#716869'}}>{this.state.username}.</span>  Your base location is <span style={{color: '#716869'}}>{this.state.baseLocation}</span>.  {this.state.currentCity ? "You are now in " + this.state.currentCity + "." : ''}
                            </p>
                        </h6>
                    </div>
                </div>
                <form>
                    <div className="row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>From</h6></label>
                            <input type="date" className="form-control" min={this.state.today} onChange={this.onStartDateChange} max={this.state.maxSDate} value={this.state.startDate} required />
                        </div>
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">  
                            <label><h6>To</h6></label>
                            <input type="date" className="form-control" min={this.state.startDate} onChange={this.onEndDateChange} max={this.state.maxEDate} value={this.state.endDate} required />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>City</h6></label>
                            <select className="form-control" onChange={this.onCitySelected} value={this.state.selectedCity} required>
                                <option value="">Select City</option>
                                {this.listCities()}
                            </select>
                        </div>
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Location</h6></label>
                            <select className="form-control" onChange={this.onLocationSelected} value={this.state.selectedLocation} required>
                                <option value="">Select Location</option>
                                {this.listLocations()}
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Building</h6></label>
                            <select className="form-control" onChange={this.onBuildingSelected} value={this.state.selectedBuilding} required>
                                <option value="">Select Building</option>
                                {this.listBuildings()}
                            </select>
                        </div>
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Floor</h6></label>
                            <select className="form-control" id="floorSelector" onChange={this.onFloorSelected} value={this.state.selectedFloor} required>
                                <option value="">Select Floor</option>
                                <option value="ANY" id="anyOptionForFloor">Any</option>
                                {this.listFloors()}
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><h6>Wing</h6></label>
                            <select className="form-control" id="wingSelector" onChange={this.onWingSelected} value={this.state.selectedWing} required>
                                <option value="">Select Wing</option>
                                <option value="ANY" id="anyOptionForWing">Any</option>
                                {this.listWings()}
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{margin: 5}} disabled={this.state.selectedWing ? false : true} onClick={this.onBookClick}>Book Seat</button>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{margin: 5}} disabled={this.state.selectedWing ? false : true} onClick={this.showSeats}>Show Seats</button>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{margin: 5}} onClick={this.onResetClick}>Reset</button>
                        </div>
                        <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <button className="btn btn-techm-red btn-block" style={{margin: 5}} onClick={this.goToBasic}>Basic Mode</button>
                        </div>
                    </div>
                </form>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Seat Booking Confirmation</ModalHeader>
                    <ModalBody>
                        <div id="alreadyBookedMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">
                                You already have booked a seat or have an ongoing booking in between  {this.state.startDate} and {this.state.endDate}.
                            </p>
                        </div>
                        <div id="confirmationMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">
                                Click Yes to book {this.state.returnedSpaceType} {this.state.returnedSpaceId} in {this.state.returnedWing === 'NA' ? this.state.returnedFloor + ' ODC' : this.state.returnedWing}, {this.state.returnedFloor}, {this.state.returnedBuilding} from {this.state.startDate} till {this.state.endDate}. 
                            </p>
                        </div>
                        <div id="noSpacesMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">Sorry, there are no spaces in {this.state.selectedFloor === 'ANY' ? this.state.selectedFloor + " floor" : this.state.selectedFloor}, {this.state.selectedWing === 'ANY' ? this.state.selectedWing + ' wing' : this.state.selectedWing}, {this.state.selectedBuilding}</p>
                        </div>
                        <div id="postBookingMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">
                                {this.state.returnedSpaceType} {this.state.returnedSpaceId} has been booked for you from {this.state.startDate} till {this.state.endDate}.  The booking details will be mailed to you shortly.{'\n\n'}
                                {this.state.customerGroup !== this.state.returnedCustomerGroup ? "Your customer group does not match with the customer group of the seat.  Please contact the Location SPOC mentioned in the confirmation email for more details" : null}
                            </p>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <div id="confirmationBox" style={{display: 'none'}}>
                            <button className="btn btn-techm-red" style={{margin: 5}} onClick={this.toggleModal}>No</button>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.bookSeat}>Yes</button>
                        </div>
                        <div id="noSpacesBox" style={{display: 'none'}}>
                            <button className="btn btn-techm-red" style={{margin: 5}} onClick={this.toggleModal}>&times; Close</button>
                        </div>
                        <div id="postBookingBox" style={{display: 'none'}}>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.goToViewBookings}>View Bookings</button>
                        </div>
                        <div id="alreadyBookedBox" style={{display: 'none'}}>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.toggleModal}>OK</button>
                        </div>
                    </ModalFooter>
                </Modal>
                <div className="row justify-content-center">
                    <div className="col-sm-12 col-xs-12">
                        <div id="listSeats" style={{display: 'none'}}>
                            {this.listSeats()}
                        </div>
                    </div>
                </div>
                <div className="row" id="viewSeatSection" style={{display: 'none'}}>
                    <SeatViewerComponent sid={this.state.sidToViewer} />
                </div>
            </section>
        );
    }
}
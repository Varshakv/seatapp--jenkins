import React, { Component } from 'react';
import axios from 'axios';
import endpoints from '../resources/config';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class ViewBookingsComponent extends Component {
    constructor(props){
        super(props);

        this.renderButton = this.renderButton.bind(this);
        this.showReleaseModal = this.showReleaseModal.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.releaseSeat = this.releaseSeat.bind(this);

        this.state = {
            lanid: '',
            bookings: [],
            today: '',
            showModal: false,
            brnToRelease: ''
        };
    }

    componentDidMount(){
        this.setState({lanid: this.props.infoToVBC});
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if(dd < 10){
            dd = '0' + dd;
        }
        if(mm < 10){
            mm = '0' + mm;
        }
        let dateString = yyyy + "-" + mm + "-" + dd;
        this.setState({today: dateString});
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getUserBookingDetails/' + this.props.infoToVBC.substr(4, 6))
            .then(response => {
                this.setState({bookings: response.data});
            }).catch(err => {
                console.log(err);
            });
    }

    showBookings(){
        if(this.state.bookings.length === 0){
            return(
                <div style={{textAlign: 'center'}}>
                    <h6>You have no bookings yet!</h6>
                </div>
            );
        } else {
            return(
                <div>
                    <div style={{textAlign: 'center'}}>
                        <h6>Your bookings</h6>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-striped table-hover table-bordered">
                            <thead className="thead-light">
                                <tr style={{textAlign: 'center'}}>
                                    <th><h6>BRN</h6></th>
                                    <th><h6>Start Date</h6></th>
                                    <th><h6>End Date</h6></th>
                                    <th><h6>Space ID</h6></th>
                                    <th><h6>Location</h6></th>
                                    <th><h6>Action</h6></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.listBookings()}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }

    renderButton(bsd, brn){
        let dateToday = new Date(this.state.today);
        let datebsd = new Date(bsd);
        return <button className="btn btn-link" disabled={ dateToday <= datebsd ? false : true } onClick={this.showReleaseModal} value={brn}>Release Seat</button>
    }

    showReleaseModal(e){
        this.setState({
            showModal: true,
            brnToRelease: e.target.value
        });
    }

    toggleModal(e){
        this.setState({
            showModal: !this.state.showModal,
            brnToRelease: ''
        });
    }

    releaseSeat(e){
        let brn = this.state.brnToRelease;
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/deleteBooking/' + brn)
            .then(response => {
                this.setState({
                    showModal: false,
                    bookings: this.state.bookings.filter(booking => booking.bookingrequestnumber != brn)
                });
            }).catch(err => {
                console.log(err);
            });
    }
    
    listBookings(){
        return this.state.bookings.map(function(record, i){
            return(
                <tr style={{textAlign: 'center'}} key={i}>
                    <td>{record.bookingrequestnumber}</td>
                    <td>{record.bookingstartdate}</td>
                    <td>{record.bookingenddate}</td>
                    <td>{record.spaceid}</td>
                    <td>{record.location}</td>
                    <td>{this.renderButton(record.bookingstartdate, record.bookingrequestnumber)}</td>
                </tr>
            );        
        }, this);
    }

    render(){
        return(
            <section className="container container-fluid" style={{marginTop: 20}}>
                <div className="col-md-12">
                    <div id="showBookings">
                        {this.showBookings()}
                        <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                            <ModalHeader toggle={this.toggleModal}>Seat Release Confirmation</ModalHeader>
                            <ModalBody>
                                <div className="justify-content-center">
                                    <p>Are you sure you want to release this seat?</p>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <button className="btn btn-techm-red" style={{margin: 5}} onClick={this.toggleModal}>No</button>
                                <button className="btn btn-primary" style={{margin: 5}} onClick={this.releaseSeat}>Yes</button>
                            </ModalFooter>
                        </Modal>
                    </div>
                </div>
            </section>
        );
    }
}
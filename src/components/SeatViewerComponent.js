import React, { Component } from 'react';
import endpoints from '../resources/config';

export default class SeatViewerComponent extends Component {

    render(){
        return(
            <section style={{marginTop: 20}}>
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src={endpoints.viewerHN + ":" + endpoints.viewerPort + '/vr?a=' + this.props.sid}></iframe>
                </div>
            </section>
        );
    }
}
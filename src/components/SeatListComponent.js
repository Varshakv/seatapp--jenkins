import React, { Component } from 'react';
import axios from 'axios';
import endpoints from '../resources/config';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class SeatListComponent extends Component {
    constructor(props){
        super(props);

        this.goToView = this.goToView.bind(this);
        this.showConfirmationModal = this.showConfirmationModal.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.bookSeat = this.bookSeat.bind(this);
        this.goToViewBookings = this.goToViewBookings.bind(this);

        this.state = {
            aid: '',
            sd: '',
            ed: '',
            sid: '',
            st: '',
            loc: '',
            sdidid: '',
            brn: '',
            acg: '',
            scg: '',
            showModal: false,
            wing: '',
            floor: '',
            building: '',
            username: ''
        };
    }

    componentDidMount(){
        this.setState({
            aid: this.props.infoToSLC.lanid,
            sd: this.props.infoToSLC.startDate,
            ed: this.props.infoToSLC.endDate,
            sid: this.props.infoToSLC.space.space.spaceid,
            st: this.props.infoToSLC.space.space.spacetype,
            sdid: this.props.infoToSLC.space.space.spacedetailid,
            loc: this.props.infoToSLC.location,
            username: this.props.infoToSLC.username,
            acg: this.props.infoToSLC.customerGroup,
            scg: this.props.infoToSLC.space.space.customer_group,
            brn: Math.floor(Date.now() / 1000)
        });
        axios.get(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/getLocationDetails/' + this.props.infoToSLC.space.space.spaceid + '/' + this.props.infoToSLC.space.space.spacedetailid)
            .then(response => {
                this.setState({
                    wing: response.data.wing,
                    floor: response.data.floor,
                    building: response.data.building
                });
            }).catch(err => {
                console.log(err);
            });
    }

    goToView(e){
        var info = {
            id: 'ViewSeat',
            spaceid: this.state.sid
        }
        this.props.callbackFromAdvanced(info);
    }

    showConfirmationModal(e){
        this.setState({showModal: true});
    }

    toggleModal(e){
        this.setState({showModal: !this.state.showModal});
    }

    bookSeat(e){
        var info = {
            "bookingrequestnumber": this.state.brn,
            "associateid": this.state.aid.substr(4, 6),
            "bookingstartdate": this.state.sd,
            "bookingenddate": this.state.ed,
            "spaceid": this.state.sid,
            "location": this.state.loc,
            "spacedetailid": this.state.sdid
        };
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'custom/book', info)
            .then(response => {
                if(response.data === 1){
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('alreadyBookedMsg').style.display = 'block';
                    document.getElementById('alreadyBookedBox').style.display = 'block';
                } else if(response.data === 2){
                    var mailerObject = {
                        recipient: this.state.aid,
                        spaceType: this.state.st,
                        spaceid: this.state.sid,
                        building: this.state.building,
                        wing: this.state.wing,
                        floor: this.state.floor,
                        startDate: this.state.sd,
                        endDate: this.state.ed,
                        locationName: this.state.loc,
                        username: this.state.username,
                        brn: this.state.brn
                    };
                    axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'mail', mailerObject)
                        .then(response => {
                            (response.data === 1 ? console.log("Mail sent") : console.log("Mail not sent"));
                        }).catch(err => {
                            console.log(err);
                        });
                    document.getElementById('confirmationMsg').style.display = 'none';
                    document.getElementById('confirmationBox').style.display = 'none';
                    document.getElementById('postBookingMsg').style.display = 'block';
                    document.getElementById('postBookingBox').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
            });
    }

    goToViewBookings(e){
        var info = {
            id: 'ViewBookings'
        };
        this.props.callbackFromAdvanced(info);
    }

    render(){
        return(
            <tr style={{textAlign: 'center', fontSize: 'small'}}>
                <td>{this.state.sid}</td>
                <td>{this.state.st}</td>
                <td>{this.state.loc}</td>
                <td>{this.state.building}</td>
                <td>{this.state.floor}</td>
                <td>{this.state.wing === 'NA' ? this.state.floor + ' ODC' : this.state.wing}</td>
                <td>
                    <button className="btn btn-link" onClick={this.goToView}>View Seat</button>
                </td>
                <td>
                    <button className="btn btn-link" onClick={this.showConfirmationModal}>Book Seat</button>
                </td>
                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Seat Booking Confirmation</ModalHeader>
                    <ModalBody>
                        <div id="confirmationMsg">
                            <p className="justify-content-center">
                                You are about to book {this.state.st} {this.state.sid} from {this.state.sd} till {this.state.ed}. Are you sure you want to book this seat ?
                            </p>
                        </div>
                        <div id="postBookingMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">
                                {this.state.st} {this.state.sid} has been booked for you from {this.state.sd} till {this.state.ed}.  The details of the booking will be mailed to you shortly. {'\n\n'}
                                {this.state.acg !== this.state.scg ? "Your customer group does not match with the customer group of the seat.  Please contact the Location SPOC mentioned in the confirmation email for more details" : null}
                            </p>
                        </div>
                        <div id="alreadyBookedMsg" style={{display: 'none'}}>
                            <p className="justify-content-center">
                                You already have booked a seat or have an ongoing booking in between  {this.state.sd} and {this.state.ed}.
                            </p>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <div id="confirmationBox">
                            <button className="btn btn-techm-red" style={{margin: 5}} onClick={this.toggleModal}>No</button>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.bookSeat}>Yes</button>
                        </div>
                        <div id="postBookingBox" style={{display: 'none'}}>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.goToViewBookings}>View Bookings</button>
                        </div>
                        <div id="alreadyBookedBox" style={{display: 'none'}}>
                            <button className="btn btn-primary" style={{margin: 5}} onClick={this.toggleModal}>OK</button>
                        </div>
                    </ModalFooter>
                </Modal>
            </tr>
        );
    }
}
import React, { Component } from 'react';
import axios from 'axios';
import endpoints from '../resources/config';
import logo from './logo.png';
import connected from './connectedLogo.jpg';

export default class LoginComponent extends Component {
    constructor(props){
        super(props);

        this.onChangeEmailID = this.onChangeEmailID.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onLoginClick = this.onLoginClick.bind(this);
        this.onResetClick = this.onResetClick.bind(this);

        this.state = {
            emailid: '',
            password: '',
        };
    }

    onChangeEmailID(e){
        this.setState({emailid: e.target.value});
    }

    onChangePassword(e){
        this.setState({password: e.target.value});
    }

    onLoginClick(e){
        e.preventDefault();
        axios.post(endpoints.backendHN + ':' + endpoints.backendPort + '/' + 'login', { "emailid": this.state.emailid, "pwd": this.state.password })
            .then(response => {
                // console.log(response.data);
                if(response.data.id === 1){
                    this.props.callbackFromApp({
                        emailid: this.state.emailid,
                        username: response.data.displayName,
                        customer_group: response.data.customer_group
                    });
                } else {
                    this.setState({ lanid: '', password: ''});
                    document.getElementById('errorMsg').style.display = 'block';
                }
            }).catch(err => {
                console.log(err);
                this.setState({emailid: '', password: ''});
            });
    }

    onResetClick(e){
        this.setState({emailid: '', password: ''});
    }

    

    render() {
        return(
            <section>
                <div className="row">
                    <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <img src={connected} alt="img" width="100%" height="102%"/>
                    </div>
                    <div className="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div className="container container-fluid" style={{marginTop: 20 }}>
                            <div className="row">
                                <img src={logo} alt="TechM Logo" style={{height: 96, width: 236}}/>
                            </div>
                            <div className="row" style={{marginTop: 45}}>
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <form onSubmit={this.onLoginClick}>
                                        <h6>
                                            <p style={{color:"#817375", fontSize: 'x-large'}}>My Seat Finder Application </p>
                                            <p style={{color: 'red', display: 'none'}} id="errorMsg">
                                                The Email ID and/or Password entered is incorrect.  Please Try Again.
                                            </p>
                                        </h6>
                                        <div className="form-group">
                                            <input type="email" className="form-control" value={ this.state.emailid } onChange={ this.onChangeEmailID } placeholder="example@TechMahindra.com" required/>
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" value={ this.state.password } onChange={ this.onChangePassword } placeholder="Password" required/>
                                        </div>
                                        <div className="form-group">
                                            <input type="submit" className="btn btn-primary" value="Sign in" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="row">
                                <h6>
                                    <p style={{fontSize: 'medium', marginTop: 35, padding: 15}}>
                                    Sign-in requires format as LANID@TechMahindra.com.
                                    </p>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
FROM node

# Create a directory where our app will be placed
RUN mkdir -p /seatapp--frontend

# Change directory so that our commands run inside this new directory
WORKDIR /seatapp--frontend

# Get all the code needed to run the app
COPY . .

# Set proxy for npm
RUN npm config set https-proxy http://ar008997:Uuuuuuuu1@10.254.40.122:8080
RUN npm config set http-proxy http://ar008997:Uuuuuuuu1@10.254.40.122:8080

# Install dependecies
RUN npm install --verbose
RUN npm install --verbose -g serve

# Build the application
RUN npm run build

# Expose the port the app runs in
EXPOSE 3000

# Serve the application
CMD ["serve","-s","build","-l","3000"]
